<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>downloads.srcttp.org - list</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <header>
            <div class="navbar navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <a class="brand" href="http://srcttp.org"><strong>SRCTTP - Downloads</strong></a> 
                        <div class="nav nav-collapse">
                            <ul class="nav">
                                <li><a href="index.php">Main</a></li>
                                <li class="active"><a href="list.php">List</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
