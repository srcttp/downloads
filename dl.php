<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>downloads.srcttp.org</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <header>
            <div class="navbar navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <a class="brand" href="http://srcttp.org"><strong>SRCTTP - Downloads</strong></a>
                        <div class="nav nav-collapse">
                            <ul class="nav">
                                <li><a href="index.php">Main</a></li>
                                <li><a href="list.php">List</a></li>
                                <?php
                                    if($_GET["g"] != "")
                                        echo "<li class='active'><a href='dl.php'>View Group \"".$_GET["g"]."\"</a></li>";
                                    else
                                        echo "<li class='active'><a href='dl.php'>Download File</a></li>";
                                ?>
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <content>
            <div class="container-fluid">
                <div class="span9">
                    <?php
                        //Output the standard text, if there is no file specified
                        //And include the current featured files
                        if($_GET['f'] == "" && $_GET['g'] == "")
                        {
                            include("include_download_a_file.html");
                            include("featuredFiles.php");
                        }
                        if($_GET['g'])
                        {
                            if(is_dir("groups/".$_GET['g']))
                                include("groups/".$_GET['g']."/index.php");
                            else
                                echo "<h1>There was an error...</h1><div class='alert alert-error'> The group \"".$_GET['g']."\" does not exist!</div>";
                        }
                    ?>
                </div>
                <div class="span3">
                    <?php
                        if($_GET['f'] == "" && $_GET['g'] == "")
                        {
                            
                        }
                    ?>
                </div>
            </div>
        </content>
        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
